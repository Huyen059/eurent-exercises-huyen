# NS course - EU Rent exercises

## Table of Contents

Day 1

1. [Modify the menu](#ex-1.1-modify-the-menu)
2. [Validation](#ex-1.2-validation)
3. [Data access control](#ex-1.3-data-access-control)
4. [Projections](#ex-1.4-projections)
5. [Calculated fields](#ex-1.5-calculated-fields)

Day 2

1. [Workflow](#ex-2.1-workflow)
2. [Tasks](#ex-2.2-tasks)
3. [Commands](#ex-2.3-commands)
4. [Custom finder](#ex-2.4-custom-finder)

## Ex 1.1: Modify the menu

### Problem

We want to modify our menu to make it more user friendly.

### Exercise - split Rentwork dropdown

You can find the menu file in control/gen/struts2/resources/menu/euRent.menu

See if you can make a submenu **bookings** with _Booking_, _Rental_ and _Car_ and a submenu **administration** with the rest of the links.

> hint: the 'name' attribute of a submenu is a free string field.

## Ex 1.2: Validation

### Problem

We want a way to validate user input and to provide feedback when the validation produces errors.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### Anchors

The easiest way to add custom code is to place it between _anchors_. For example the following create-method has 2 anchor-pairs. Between the _start_ and _end_ anchors custom code can be placed. After harvesting, the code will be injected between the anchors during expansion.

```java
public CrudsResult<DataRef> create(ParameterContext<CarDetails> detailsParameter) {
   CarDetails details = detailsParameter.getValue();
   UserContext userContext = detailsParameter.getUserContext();

   // anchor:custom-preCreate:start
   // anchor:custom-preCreate:end

   CrudsResult<DataRef> result = carCrudsLocal.create(detailsParameter);

   if (result.isError()) {
     return result;
   }
   // anchor:custom-postCreate:start
   // anchor:custom-postCreate:end
   return result;
}
```

#### CrudsResult

Instead of throwing an exception when a validation fails or something goes wrong, methods return a CrudsResult or a variation of this in order to represent the error state.

When the result is a success, the CrudsResult wraps a return-value, which can be retrieved with the getValue()-method.

```java
CrudsResult<DataRef> success = CrudsResult.success(DataRef.withId(0L));
DataRef dataRef = success.getValue();
```

When the result is an error, the CrudsResult can be defined with a number of error messages.

```java
CrudsResult<DataRef> error = CrudsResult.error(
      Diagnostic.error("rentWork", "car", "rentWork.car.demoError")
);
```

Diagnostics represent error messages. They are defined with 3 parameters:

1. **component:** the name of the component, _rentWork_ in this case
2. **element:** the name of the element, _car_ in this case
3. **key:** a key that uniquely defines the error message you wish to propagate, the convention is to define it as `component.element.errorDescription` or `component.element.field.errorDescription`

There are also some helper classes that can be used to create cruds error with error messages.

```java
// DiagnosticFactory can create diagnostics
DiagnosticFactory diagnosticFactory = new DiagnosticFactory("rentWork", "car");
Diagnostic errorMsg = diagnosticFactory.error("rentWork.car.demoError");

// DiagnosticHelper can create crudsErrors with diagnostics
DiagnosticHelper diagnosticHelper = new DiagnosticHelper("rentWork", "car");
CrudsResult<DataRef> crudsError = diagnosticHelper.createCrudsError("rentWork.car.demoError");

// DiagnosticFieldFactory can be used to create diagnostics on field level
DiagnosticFieldFactory fieldFactory = diagnosticFactory.fieldFactory("licensePlate");
Diagnostic fieldErrorMsg = fieldFactory.error("rentWork.car.licensePlate.demoError");
```

#### Translation

Error messages are defined by a key and are translated for the user. Messages can be defined in a .properties-file for each language. Look for the rentWork.properties file in the control layer.

```properties
rentWork.car.demoError=An error occurred
rentWork.car.licensePlate=Something is wrong with this licensePlate
```

### Example - Car: license plate

In the example, we make a validation to check whether a license plate provided by the user does not contain any strange characters.

You can find this customization in the _CarBean_ class under the _custom-preCreate_ anchor.

### Exercise - Booking: check bookingDate

We don't want our users to make bookings on dates that have already past, hence we only want to allow the creation of bookings with dates that occur after today.

1. Add a 'bookingDate' field to the Booking data-element. Re-expand the application to add the field to the BookingDetails class.

2. Add a validation to the booking element that checks if the booking date of a newly created element is in the future. Use the _diagnosticHelper_ to generate crudsErrors.

3. Make sure that the error message is properly translated.

4. When the validation returns an error message, the message is presented as a global error instead of a field error. Change the validation so that the error is shown as a specific error for the bookingDate field.

#### Extra exercise

Note that when editing a booking, we might need to change details of a booking that has already past. Add a validation that checks the booking date on modification only when the booking date has changed.

## Ex 1.3: Data access control

### Problem

We want to filter the data to only show data that is applicable for the current user.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### ParameterContext

All parameters in the expanded code are wrapped with a ParameterContext-object. This allows us to pass context about the execution in a version transparent way. For example, the ParameterContext object holds a UserContext object that contains information about the user.

To construct a ParameterContext object use the factory method or use another ParameterContext object to construct it:

```java
ParameterContext<CarDetails> param1 = ParameterContext.create(userContext, carDetails);
ParameterContext<CarDetails> param2 = param1.construct(carDetails);
```

#### UserContext

In order to retrieve information about the current user, it is possible to use the UserContext object that is passed with each ParameterContext object.

```java
UserContext userContext = param.getUserContext();
Long userId = userContext.getId();
String language = userContext.getLanguage();
String userName = userContext.getUserName();
```

#### Query builder

In the NS framework, queries are constructed with the help of a query builder. The queryBuilder takes queryParameter objects that are defined as follows:

```java
String name = "test";
QueryParameter queryParam = QueryParameter.createStringParameter("name", "=", "Name", name);
queryBuilder.addParameter(queryParam);
```

The 4 parameters of the createStringParameter method should be:

1. **fieldName:** name of the field which we want to compare
2. **operator:** operator that is used for the comparison (e.g. "=", ">", "LIKE" ...)
3. **parameterName:** name of the parameter as it will be used in the query. This should not collide with other parameter-names in the query. By convention this should be the name of the field in PascalCase (e.g. licensePlate -> LicensePlate)
4. **value:** the actual value of the parameter

> The query for each finder is expanded by default, but there are anchors for if it is necessary to add extra constraints.

### Example - Car: filter by owner

In the example we add a parameter to all database queries for the Car element that filters the result to only the elements that have an owner corresponding to the current user. Note that the owner field is linked to the User element of the account component.

You can find the customization in the _CarFinderBean_ class under the _custom-fetch-before-create-queries_ anchor;

### Exercise - Booking: filter by enteredBy

The enteredBy field is a special field that is filled on creation with a reference to the user that created it. We can use this field to make sure that users only see the bookings they themselves have made.

1. Add a linkField 'enteredBy' field to Booking. The field should link to the User data-element of the component 'account'.
2. Add a filter on the bookings to reduce the results of each query to the bookings entered by the current user.
3. Switch between different users to see if you can create bookings that are only visible to the current user

> Some users and passwords to try out:

> user  | password
> ----- | ---------
> super | testSuper
> admin | testAdmin
> user1 | testUser1

## Ex 1.4: Projections

### Problem

We want to control the information presented to the user for a certain data element.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### Projections

Each instance of a dataElement can be presented in the form of a projection. The use of these projections is to create a certain view of the data.

There are 3 default projections:

1. **dataRef:** A dataRef is the most barebones projection with only an id and name, along with some other data about the dataElement
2. **details:** The details projection contains all fields as they are stored in the database. Only calculated fields are not included in the details projection.
3. **info:** The info projection contains all fields marked as info fields. These are also the fields shown in the standard table on the elements page. As of _version 3.1.4_ of the expanders, the info projection can also contain calculated fields.

### Example - Car: overview

In the example we've created a view that shows the 'details' represents of a car when one is selected.

The model for the projection can be found in the Prime Radiant under the Car data-element. The customization can be found in the _car-ko-page.js_ and _car-ko-page.jsp_-files.

### Exercise - Booking: overview

The booking has an enteredBy field that we would like to hide from the user.

1. Define a projection _overview_ in the Prime Radiant for Booking. It should have the fields: name, bookingDate, carCategory and person.
2. Expand the application and look for the _BookingOverview_ class. It should be a DTO class containing the 4 defined fields.
3. Implement a customization so that we see the overview projection when a booking is selected. Show the person and booking data in a header above the details view.

## Ex 1.5: Calculated fields

### Problem

We want to calculate information based on the data and present this information to the user.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### SearchDetails

To search the database, a finder should be defined and wrapped with a SearchDetails object. The finder object is a DTO that represents the parameters for a query.

The SearchDetails object defines the projection in which the results should be returned and the paging. The paging defines how many results the query should return (default is 10).

```java
CarFindByLicensePlateEqDetails finder = new CarFindByLicensePlateEqDetails();
finder.setLicensePlate("1-AAA-777");

SearchDetails<CarFindByLicensePlateEqDetails> searchDetails = new SearchDetails<...>(finder);
searchDetails.setProjection("details");
searchDetails.setPaging(Paging.fetchAll());

SearchResult<CarDetails> searchResult = find(parameterContext.construct(searchDetails));
```

Find returns a SearchResult object. This will be explained in the next segment.

#### SearchResult

The find method returns a SearchResult object. Like the CrudsResult, it is a way to return either a failed state or a success state. If a success, it can return the results and a total number of items that satisfy the query.

```java
if (searchResult.isError()) {
    ...
}

List<CarDetails> results = searchResult.getResults();
int totalNumberOfItems = searchResult.getTotalNumberOfItems();
Option<CarDetails> first = searchResult.getFirst();
```

> Note that if the paging is defined to only return a limited amount of results (say 7), then the _getTotalNumberOfItems_-method still returns the real number of items even if it exceeds this limit (e.g. 100).

> Also note that if no items in the database satisfy the query, this is not considered an error state. The SearchResult will be a success with an empty result list and a total number of items of 0\. The correct way of checking if the resultset is non-empty is:

> ```java
> boolean hasResult = searchResult.getTotalNumberOfItems() > 0;
> /* OR */
> boolean hasResult = searchResult.getFirst().isDefined();
> ```

### Example - Car: average price

In the example we define the _'average price'_ field and implement the calculation of the average price for each car.

The customizations can be found in the _CarCruds_ class under the _custom-calculation-averagePrice_ anchor

### Exercise - Booking: number of cars

Each booking has a number of rentals assigned to it. The user wants to see how many rentals have been assigned to each booking.

1. Define a calculated field numberOfCars with valueType 'Integer' and add it to the overview projection.
2. Implement the calculateNumberOfCars method by counting the number of Rental-instances that are linked to the booking instance. Make sure the result is not skewed by the paging!

## Ex 2.1: Workflow

### Problem

We want to perform a sequence of steps.

### Important concepts

#### State Task

Workflows are realized by keeping a status field with each instance. Instances are then moved from state to state by executing a task and setting the appropriate _end state_. Such a state transition is defined in a _State Task_. StateTasks have the following properties:

1. **name:** Name of task that is to be executed.
2. **processor:** A combination of the component and task-name (e.g. _rentWork:AssignFleetLocation_ where rentWork is the component and AssignFleetLocation is the name of the task).
3. **implementation:** The implementation class used for the task. The expanders generate 2 implementation classes by default with postfixes _-Impl_ and _-Impl2_
4. **params:** A way to further parameterize the implementation class.
5. **begin state:** Instances that contain this state will be picked up by the workflow to perform this state task.
6. **interim state:** Instances picked up by the workflow receive this state while they are being processed.
7. **failed state:** If the task fails, the instance will receive this state.
8. **end state:** If the task is successful, the instance will receive this state.

### Example - CarFlow

As an example, a 'CarFlow' has been defined that assigns a fleet location to each car. You can find the configuration of the workflow under the workflow page.

### Exercise - BookingFlow

Each booking has to be validated and if valid an invoice should be created. During this exercise, we will focus on setting up the flow and leave the implementation for the next exercise. Some dummy implementations have already been provided for the _ValidateBooking_ and _MakeBookingInvoice_ tasks.

1. Create a _BookingFLow_ flow element that targets _Booking_
2. Create the _ValidateBooking_ and _MakeBookingInvoice_ tasks in the Prime Radiant. Look at the AssignFleetLocation task as an example.
3. In the deployed application, create a stateTask that moves bookings from the **Created** state to the **Valid** state. Use the _eurent.tutorial.ValidateBookingImpl2_ class as implementation for the _ValidateBooking_ task.
4. Create a stateTask that moves bookings from the **Valid** state to the **Invoice created** state. Use the _eurent.tutorial.MakeBookingInvoiceImpl2_ class as implementation.

![Booking flow diagram](booking-flow-diagram.png)

## Ex 2.2: Tasks

### Problem

We want to perform a piece of logic that can be executed in a workflow or as a user action.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### Agents

In order to access the beans in a technology-agnostic way, the NS-framework provide agents. Agents can be accessed with a static constructor:

```java
CarLocalAgent carAgent = CarLocalAgent.getCarAgent(userContext);
```

The agents expose a number of methods provided by the beans. For example, to create a new Car instance, use:

```java
CrudsResult<DataRef> createResult = carAgent.create(carDetails);
```

### Example - Car: AssignFleetLocation

As an example, the _AssignFleetLocation_ task has been implemented. This task will assign the car to a fleet location that has not reached maximum capacity yet.

The implementation can be found in the _AssignFleetLocationImpl_ class. You can run the task by starting the engine and adding some cars with the 'ToBeAssigned' status.

### Exercise - Booking ValidateBooking and MakeBookingInvoice tasks

We will now implement the tasks used in the workflow of the previous exercise.

1. Implement the _eurent.tutorial.ValidateBookingImpl_ to only allow bookings that have at least 1 rental. Also make sure that for each rental the price is defined. Return a TaskResult.error if the Booking is not valid.
2. Implement the _eurent.tutorial.MakeBookingInvoiceImpl_ so that it makes the sum of the price of all rentals and stores it in a BookingInvoice instance. Also link the invoice to the booking.

> You can modify the stateTasks in order to test your implementations.

#### Extra exercise

Modify _eurent.tutorial.ValidateBookingImpl_ so that instead of failing when the price of a rental is not defined, it uses the _dayPrice_ field of the linked CarModel instance to set the price of the rental.

E.g. the user makes a rental with car "1-AAA-777" that has as model "Audi A4". The price is not defined, so during validation it takes the dayPrice of the "Audi A4" model, which is 50.

## Ex 2.3: Commands

### Problem

We want define non-standard actions a user can perform on our system.

### Position in NS architecture

In which layer does this customization belong?

- view
- control
- proxy
- logic
- data
- shared

### Important concepts

#### Ext folder

Each layer in the expanded code has a _gen_ folder and an _ext_ folder. The gen folders contain all files generated by the expanders.

The ext folder on the other hand can be used for external files made by the developer. Files in the ext folders are harvested and placed back in their original place after expansion.

### Example - Car: resetFleetLocation

In the example we implemented the resetFleetLocation command.

The anchor for the command can be found in the _resetFleetLocation_ method of the _CarBean_ class. We placed the actual implementation in a separate class named _eurent.tutorial.CarFleetLocationResetter_, which is placed in the ext folder of the logic layer.

You can find the implementation for the button in the _car-table-view.js_ file.

### Exercise - Booking: startValidation

Our booking workflow works as intended. There is however 1 issue: we barely have the time to enter our bookings and accompanying rentals before the workflow tries to validate them. That's why we need a separate state in between that we can set manually.

1. Modify the StateTasks for the BookingFlow so that it starts with **ToBeValidated** instead of **Created**
2. Add the _startValidation_ command to the Booking data-element in the Prime Radiant. The 'has target instance' setting will make sure the command is executed on one instance of a booking.
3. Implement the _startValidation_ command so that it sets the state of the selected booking to **ToBeValidated**. Also check if the selected booking has the state **Created** and return an error otherwise.
4. Add a button to the Booking page to kick-start our command.

## Ex 2.4: Custom finder

### Problem

We want to use a finder that is nor supported by standard finders

### Important concepts

#### IsCustomFinder Option

The _isCustomFinder_ is an option on FinderElement that places anchors at the right places so that

### Example - CarFindByDayPriceBetween

Let's say the users of our EU Rent application want to find all cars with a day-price within a certain range. The day-price of a car is defined by its car-model.

To implement this finder an number of files have been modified:

1. In the Prime Radiant, the finder "findByDayPriceBetween" has been added with the option _isCustomFinder_.
2. In the eurent.tutorial.CarFindByDayPriceBetweenDetails class, 2 fields have been added to pass the lowest and highest price for a car.
3. In /view/gen/knockout/js/rentWork/carFindByDayPriceBetween-ko-mapper.js, the 2 mapping-functions, jsToViewModel and viewModelToJS, have been modified to map these 2 new fields between viewmodel and a representation that can be sent to the backend.
4. In /view/gen/knockout/html/rentWork/carFindByDayPriceBetween-ko-form.html, the form for filling in the finder has been created so that users can use the finder on the Car page.
5. In the eurent.tutorial.CarData class, 2 named queries have been added to find/count the data.
6. In eurent.tutorial.CarFinderBean#findByDayPriceBetween, the named queries are used to fetch the data for the finder

### Exercise - BookingFindByBookingDateEq_CarEq

For this exercise, create a finder for the Booking element that finds all bookings that are booked for a certain date and rent a specific car (i.e. a Rental exists that is linked to the booking and the selected car).

#### Extra exercise

Implement an additional validation in the _eurent.tutorial.ValidateBookingImpl_ task so that the same car is not added to 2 different bookings on the same booking date.

Use your new finder for booking to find other bookings on the same date with the same car.
