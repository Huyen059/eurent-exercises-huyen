# Setup for EU Rent

To start, extract the EURENT.zip folder into your workspace (C:/NSF/workspace). The folder structure should be as follows:

```
NSF
  '-- workspace
        '-- EURENT
              '-- applications
              '-- components
			  '-- ...
```

The EURENT folder contains the basis of a application called 'EU Rent'. We will be using this application for our exercises.

## Step 1: synchronize NsfBase

When opening the Prime Radiant go to `Configuration > NsfBase`. Once there, press the _synchronize all_ button

## Step 2: import the application

Go to `Dashboard > Application`. There you can find an _import_ button. When pressed, it will prompt for a _SourceBase_. Select _EU Rent_ in the dropdown and press _confirm_.

## Step 3: verify

If the import was successful the PR should contain an application with the name _EU Rent: Exercises_ and a component with the name _rentWork_.

## Step 4: create deployment instance and select a deploy base

Go to `Dashboard > Application`. Select the application instance _EU Rent: Exercises_. Under the _application_ tab you choose the _Application instance_ tab. Select the shown EU Rent instance. When no deployment is shown you request a new one using the _request deploy_ button. Edit the deployment instance (button with pencil icon) and select a deploy base.

## Step 5: expand, build and deploy

Go to `Dashboard > Expansion`. Select the application instance _EU Rent: Exercises_ and execute the following actions in sequence:

- **export**: this will export the application and related components, settings and expansion-settings to the directory linked to the custom base of the ApplicationInstance
- **generate menu harvest**: 
- **expand**: this generate the expanded files: a new java project will be generated based on the data in the PR
- **build**: this will build the java project and generate a new .ear file that can be deployed for testing

After this, select the _Application deployment_ tab and execute:

- **generate deploy configuration**: this will generate config files that are specific to your application. This includes, among other, the data-sources for each component.
- **prepare**: this will install a new database and a number of other files to prepare deployment
- **do start**: this will deploy the application

> note: When the application is deployed, the Prime Radiant will open a command window with the application log. However, the standard logging is very minimal. To show more of the log/change the type of logging,go to your NSF/infrastructure/tomee-plume-8.0.1/BASE?/conf folder where BASE? is the deploy base you have selected. The _log4j2.xml_ can be altered under _Loggers_. Additional logging per package can also be added.

## Step 6: open the project in your editor

You can find a maven application in the following folder:

```
NSF
  '-- primespace
        '-- expansionsPR
              '-- euRent-id??
                    '-- euRent
```

You can import this application into the editor.

> note (IntelliJ): The project might not compile correctly at first. To fix this, you can go to _settings_, enter maven and point the _maven home directory_ to C:/NSF-3.0/infrastructure/apache-maven-3.0.5

## Step 7: after editing custom code

In order to save written custom code you should harvest the code. Go to `Dashboard > Expansion` and press the _harvest_ button under the _expansion_ tab.