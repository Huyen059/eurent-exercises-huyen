# Setup for EU Rent

To start, extract the EURENT.zip folder into your workspace (C:/NSF-3.0/workspace). The folder structure should be as follows:

```
NSF-3.0
  '-- workspace
        '-- EURENT
              '-- applications
              '-- components
```

The EURENT folder contains the basis of a application called 'EU Rent'. We will be using this application for our exercises.

## Step 1: create a new SourceBase

When opening the Prime Radiant go to `Software Configs > SourceBase`. Once there, create a new SourceBase instance with the following details:

Field      | Value
---------- | ---------
Name       | EU Rent
Short name | euRent
Nsf base   | NSF 3.0
Subspace   | workspace
Base root  | EURENT

## Step 2: import the application

Go to `Functional Elements > Application`. There you can find an _import_ button. When pressed, it will prompt for an _application file_ and a _model repository_. You can find the application file under the following location:

```
EURENT
  '-- applications
        '-- euRent
              '-- model
                    '-- euRent
                          > euRent-1.0.xml
```

The model repository should be EU Rent, the source base we created in the previous step.

## Step 3: verify

If the import was successful the PR should contain an application with the name _EU Rent: Exercises_ and a component with the name _rentWork_.

## Step 4: select a deploy base

Go to `Software constructs > ApplicationInstance`. Select the application instance _EU Rent: Exercises_. Edit the instance (button with pencil icon) and select a deploy base.

## Step 5: expand, build and deploy

Go to `Software constructs > ApplicationInstance`. Select the application instance _EU Rent: Exercises_ and execute the following actions in sequence:

- **provision**: this will remove the expanded project (if any) and generate a new menu file and a new database file for hsqldb
- **expand**: this generate the expanded files: a new java project will be generated based on the data in the PR
- **build**: this will build the java project and generate a new .ear file that can be deployed for testing
- **request deploy**: this will create a new application deployment instance (you will only need this action once per application instance, since it just generates data)

After this, select the _Application deployment_ tab and execute:

- **clean up**: this will clean up the base in case there was some data left from another application
- **prepare**: this will install a new database and a number of other files to prepare deployment
- **restore DB**: this will take a backup DB file that already has some data configured
- **do start**: this will deploy the application

> note: When the application is deployed, the Prime Radiant will open a command window with the application log. However, the standard logging is very minimal. To show more of the log, copy the log4j.properties file to your NSF-3.0/infrastructure/jonas-full-5.1.4/BASE?/conf folder where BASE? is the deploy base you have selected.

## Step 6: open the project in your editor

You can find a maven application in the following folder:

```
NSF-3.0
  '-- primespace
        '-- expansionsPR
              '-- euRent-id??
                    '-- euRent
```

You can import this application into the editor.

> note (IntelliJ): The project might not compile correctly at first. To fix this, you can go to _settings_, enter maven and point the _maven home directory_ to C:/NSF-3.0/infrastructure/apache-maven-3.0.5
